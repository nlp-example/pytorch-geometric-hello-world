import torch

x = torch.randn(1, requires_grad=True)

print(x)
print(x.grad)

y = x ** 2

# dy/dx = 2x
y.backward()

print(y)
print(x.grad)