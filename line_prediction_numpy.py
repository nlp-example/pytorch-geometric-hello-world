import numpy as np
import matplotlib.pyplot as plt

np.random.seed(2)
lr = 0.1
n_epoch = 100

# Create data
x = np.random.rand(100, 1)
y = 1 + 2 * x + 0.1 * np.random.randn(100, 1)

plt.plot(x, y, 'o')
plt.show()

idx = np.arange(100)
np.random.shuffle(idx)

train_idx = idx[:80]
test_idx = idx[80:]

x_train = x[train_idx]
y_train = y[train_idx]

x_test = x[test_idx]
y_test = y[test_idx]

a = np.random.randn(1)
b = np.random.randn(1)

for epoch in range(n_epoch):
    # Model
    y_prediction = a + x_train * b

    # Loss
    error = y_train - y_prediction
    loss = (error ** 2).mean()
    print(loss)
    a_grad = -2 * error.mean()
    b_grad = -2 * (x_train * error).mean()

    a = a - lr * a_grad
    b = b - lr * b_grad

print(f"a = {a} | b = {b}")
