import numpy as np
import matplotlib.pyplot as plt
import torch


class LinearRegression(torch.nn.Module):
    def __init__(self):
        super(LinearRegression, self).__init__()
        self.neuron = torch.nn.Sequential(torch.nn.Linear(1, 1))

    def forward(self, x):
        y = self.neuron(x)
        return y


np.random.seed(2)
lr = 0.1
n_epoch = 1000

# Create data
x = np.random.rand(100, 1)
y = 1 + 2 * x + 0.1 * np.random.randn(100, 1)


x = torch.from_numpy(x)
y = torch.from_numpy(y)

plt.plot(x, y, 'o')
plt.show()

idx = np.arange(100)
np.random.shuffle(idx)

train_idx = idx[:80]
test_idx = idx[80:]

x_train = x[train_idx].float()
y_train = y[train_idx].float()

x_test = x[test_idx]
y_test = y[test_idx]


model = LinearRegression()
model.train()
optimizer = torch.optim.SGD(params=model.parameters(), lr=lr)

loss_function = torch.nn.MSELoss()

for epoch in range(n_epoch):
    y_prediction = model(x_train)
    loss = loss_function(y_prediction, y_train)
    loss.backward()
    optimizer.step()
    optimizer.zero_grad()

    print(loss.item())

print(model.state_dict())
