import numpy as np
import matplotlib.pyplot as plt
import torch

np.random.seed(2)
lr = 0.1
n_epoch = 1000

# Create data
x = np.random.rand(100, 1)
y = 1 + 2 * x + 0.1 * np.random.randn(100, 1)


x = torch.from_numpy(x)
y = torch.from_numpy(y)

plt.plot(x, y, 'o')
plt.show()

idx = np.arange(100)
np.random.shuffle(idx)

train_idx = idx[:80]
test_idx = idx[80:]

x_train = x[train_idx].float()
y_train = y[train_idx].float()

x_test = x[test_idx]
y_test = y[test_idx]

# a = torch.randn(1, requires_grad=True)
# b = torch.randn(1, requires_grad=True)

model = torch.nn.Sequential(torch.nn.Linear(1, 1))
model.train()

# optimizer = torch.optim.SGD(params=[a, b], lr=lr)
optimizer = torch.optim.SGD(params=model.parameters(), lr=lr)

loss_function = torch.nn.MSELoss()

for epoch in range(n_epoch):
    # y_prediction = a + x_train * b

    y_prediction = model(x_train)
    loss = loss_function(y_prediction, y_train)
    loss.backward()
    optimizer.step()
    optimizer.zero_grad()

    # error = y_train - y_prediction
    # loss = (error ** 2).mean()
    # print(loss)

    # loss.backward()

    # a_grad = -2 * error.mean()
    # b_grad = -2 * (x_train * error).mean()

    # a = a - lr * a_grad
    # b = b - lr * b_grad

    print(loss.item())


# print(f"a = {a.item()} | b = {b.item()}")
print()
print(model.state_dict())
